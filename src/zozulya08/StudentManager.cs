﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WebApplication1
{
    public class StudentManager
    {
        public static MyCollection<Student> students = ReadFromFile();

        public static MyCollection<Student> ReadFromFile()
        {
            MyCollection<Student> students = new MyCollection<Student>();
            var path = @"D:\students.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });
                        var obj = new Student();
                        obj.LastName = infoStudent[0];
                        obj.Initials = infoStudent[1];
                        obj.SetBirthday(DateTime.Parse(infoStudent[2]));
                        obj.SetEnterDate(DateTime.Parse(infoStudent[3]));
                        obj.IndexGroup = infoStudent[4];
                        obj.Faculty = infoStudent[5];
                        obj.Specialization = infoStudent[6];
                        obj.AverageRating = int.Parse(infoStudent[7]);
                        students.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return students;
        }

    }
}
