﻿using System;

namespace WebApplication1
{
    [Serializable]
    public class Student
    {
        public string LastName { get; set; }
        public string Initials { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime EnterDate { get; set; }
        public string IndexGroup { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int AverageRating { get; set; }
        public int Age;
        public int Course;

        public void SetBirthday(DateTime b)
        {
            Birthday = b;
            DateTime today = DateTime.Today;
            this.Age = today.Year - this.Birthday.Year;
            if ((this.Birthday.Month == today.Month && this.Birthday.Day > today.Day) || this.Birthday.Month > today.Month)
            {
                this.Age -= 1;
            }
        }
        public void SetEnterDate(DateTime e)
        {
            EnterDate = e;
            DateTime today = DateTime.Today;
            this.Course = today.Year - this.EnterDate.Year;
            if (today.Month > 9)
                this.Course++;
        }
        public override string ToString()
        {
            return $"Student: {LastName} {Initials}\nBirthday: {Birthday.ToString()}\nEnterance date: {EnterDate.ToString()}\n|{IndexGroup}|{Faculty}|{Specialization}|{AverageRating}";
        }

        public Student()
        {

        }
        public override bool Equals(object other)
        {          
            if (other == null)
                return false;
            if (object.ReferenceEquals(this, other))
                return true;
            if (this.GetType() != other.GetType())
                return false;
            return this.Equals(other as Student);
        }
        public bool Equals(Student other)
        {
            if (other == null)
                return false;
            if (object.ReferenceEquals(this, other))
                return true;
            if (this.GetType() != other.GetType())
                return false;
            if (string.Compare(this.LastName, other.LastName, StringComparison.CurrentCulture) == 0 && string.Compare(this.Initials, other.Initials, StringComparison.CurrentCulture) == 0)
                return true;
            else
                return false;
        }
        public override int GetHashCode()
        {
            var hashcode = 324023398;
            hashcode = hashcode * 21312039 + Age.GetHashCode();
            hashcode = hashcode * 12324454 + LastName.GetHashCode();
            return hashcode;
        }
    }
}