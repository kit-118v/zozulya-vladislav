﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Lab4
{
    public class Menu
    {
        public MyCollection<Student> students;

        public Menu()
        {
            students = new MyCollection<Student>();
        }
        public void MainMenu()
        {
            string selection;
            var number = 0;

            while (number != 1)
            {
                printMainMenu();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            ReadStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ShowAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            PrintByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            WriteToFile();
                            break;
                        case 6:
                            Console.Clear();
                            ReadFromFile();
                            break;
                        case 7:
                            Console.Clear();
                            RedactStudent();
                            Console.Clear();
                            break;
                        case 8:
                            Console.Clear();
                            RemoveByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 9:
                            Console.Clear();
                            students.clear();
                            Console.Clear();
                            break;
                        case 10:
                            Console.Clear();
                            SearchByLastname();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 11:
                            Console.Clear();
                            Lab4Info();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }


        void Lab4Info()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if(index < 0 || index >= students.Count())
            {
                return;
            }
            Console.WriteLine("Enterence date: " + students.get(index).EnterDate);
            Console.WriteLine("Faculty: " + students.get(index).Faculty);
            Console.WriteLine("Specialization: " + students.get(index).Specialization);
            Console.WriteLine("Group index: " + students.get(index).IndexGroup);

            Console.WriteLine("\nCourse: " + students.get(index).Course);
            Console.WriteLine("\nAge: " + students.get(index).Age);
        }

        void SearchByLastname()
        {
            Console.WriteLine("Enter lastname: ");
            string str = Console.ReadLine();
            int i = 0, k = 0;
            foreach (var student in students)
            {
                if (student.LastName.IndexOf(str) >= 0)
                {
                    printStudent(student.ToString(), i);
                    k++;
                }
                i++;
            }
            if (k == 0)
                Console.WriteLine("Nothing found. ");
        }
        void printMainMenu()
        {
            Console.WriteLine("Amount of students: " + students.Count());
            Console.WriteLine("1  - Exit");
            Console.WriteLine("2  - Add new student");
            Console.WriteLine("3  - Show all students");
            Console.WriteLine("4  - Get student by index");
            Console.WriteLine("5  - Save in file");
            Console.WriteLine("6  - Read from file");
            Console.WriteLine("7  - Edit student");
            Console.WriteLine("8  - Delete student");
            Console.WriteLine("9  - Delete all students");
            Console.WriteLine("10 - Search by lastname");
            Console.WriteLine("11 - Show LAB4 info");
            Console.Write("Your choice: ");
        }
        void printEditMenu()
        {
            Console.WriteLine("1 - Exit");
            Console.WriteLine("Edit: ");
            Console.WriteLine("2 - Name");
            Console.WriteLine("3 - Initials");
            Console.WriteLine("4 - Birthday");
            Console.WriteLine("5 - Enter date");
            Console.WriteLine("6 - Index of group");
            Console.WriteLine("7 - Faculty");
            Console.WriteLine("8 - Specialization");
            Console.WriteLine("9 - Average rating");
        }
        DateTime checkDate(string text)
        {
            DateTime dt = new DateTime();
            try
            {
                Console.WriteLine(text);
                string entered = Console.ReadLine();
                return DateTime.ParseExact(entered, "MM/DD/YYYY", CultureInfo.InvariantCulture);
            }
            catch (System.FormatException e)
            {
                Console.WriteLine("Invalid date. ");
            }
            return dt;
        }
        public string checkRegex(string text, string regex)
        {
            string str = null;

            Console.Write(text);
            while (true)
            {
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, regex))
                {
                    Console.Write("Invalid input. Try again: ");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return str;
        }

        void ReadStudent()
        {
            var student = new Student();

            student.LastName   = checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
            student.Initials   = checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
            student.Birthday   = checkDate("Enter date of birthday [only 'MM/DD/YYYY' format]: ");
            student.EnterDate  = checkDate("Enter date of adding student to list [only 'MM/DD/YYYY' format]:");
            student.IndexGroup = checkRegex("Enter index of group: ", @"^[A-Za-z]$");
            student.Faculty    = checkRegex("Enter faculty: ", @"^.*");
            student.Specialization = checkRegex("Enter specialization: ", @"^.*");
            student.AverageRating = Convert.ToInt32(checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));

            students.Add(student);
        }

        void ShowAll()
        {
            int i = 0;
            Console.WriteLine("-----------------------Students-----------------------");
            foreach (var student in students)
            {
                printStudent(student.ToString(), i);
                i++;
            }
        }

        void printStudent(string str, int index)
        {
            string[] infoStudent = str.Split(new char[] { '|' });
            Console.WriteLine("Index of student: " + index);
            Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
            Console.WriteLine("Birthday: " + infoStudent[2]);
            Console.WriteLine("Enter date: " + infoStudent[3]);
            Console.WriteLine("Inedx of group: " + infoStudent[4]);
            Console.WriteLine("Faculty: " + infoStudent[5]);
            Console.WriteLine("Specialization: " + infoStudent[6]);
            Console.WriteLine("Average rating: " + infoStudent[7]);
            Console.WriteLine("");
        }
        void PrintByIndex()
        {
            int index;
            Console.Write("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= students.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            printStudent(students[index].ToString(), index);
        }

        public void WriteToFile()
        {
            var path = @"D:\students.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in students)
                    {
                        sw.WriteLine(student.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ReadFromFile()
        {
            var path = @"D:\students.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });
                        var obj = new Student();
                        obj.LastName = infoStudent[0];
                        obj.Initials = infoStudent[1];
                        obj.setBirthday(DateTime.Parse(infoStudent[2]));
                        obj.setEnterDate(DateTime.Parse(infoStudent[3]));
                        obj.IndexGroup = infoStudent[4];
                        obj.Faculty = infoStudent[5];
                        obj.Specialization = infoStudent[6];
                        obj.AverageRating = int.Parse(infoStudent[7]);
                        students.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        void RemoveByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());

            if (students.remove(index))
                Console.WriteLine("Student was successfully deleted. ");
            else
                Console.WriteLine("Failed. ");
        }

        public void RedactStudent()
        {
            if (students.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            int number = 0;
            int index = 0;
            while (true)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < students.Count() && index >= 0)
                {
                    break;
                }
            }

            printEditMenu();
            selection = Console.ReadLine();

            if (int.TryParse(selection, out number))
            {
                switch (number)
                {
                    case 1:
                        return;
                    case 2:
                        students[index].LastName = checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
                        break;
                    case 3:
                        students[index].Initials = checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
                        break;
                    case 4:
                        students[index].Birthday = checkDate("Enter date of birthday [only 'YYYY-MM-DD' format]: ");
                        break;
                    case 5:
                        students[index].EnterDate = checkDate("Enter date of adding student to list [only 'YYYY-MM-DD' format]:");
                        break;
                    case 6:
                        students[index].IndexGroup = checkRegex("Enter index of group: ", @"^[A-Za-z]$");
                        break;
                    case 7:
                        students[index].Faculty = checkRegex("Enter faculty: ", @"^.*");
                        break;
                    case 8:
                        students[index].Specialization = checkRegex("Enter specialization: ", @"^.*");
                        break;
                    case 9:
                        students[index].AverageRating = Convert.ToInt32(checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));
                        break;
                }

                Console.WriteLine("The result of redaction: ");
                printStudent(students[index].ToString(), index);
            }
        }
    }
}