﻿using System;

namespace Lab4
{
    public class Student
    {
        public string LastName { get; set; }
        public string Initials { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime EnterDate { get; set; }
        public void setBirthday(DateTime b)
        {
            Birthday = b;
            DateTime today = DateTime.Today;
            this.Age = today.Year - this.Birthday.Year;
            if ((this.Birthday.Month == today.Month && this.Birthday.Day > today.Day) || this.Birthday.Month > today.Month)
            {
               this.Age -= 1;
            }
        }
        public void setEnterDate(DateTime e)
        {
            EnterDate = e;
            DateTime today = DateTime.Today;
            this.Course = today.Year - this.EnterDate.Year;
            if (today.Month > 9)
                this.Course++;
        }
        public string IndexGroup { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int AverageRating { get; set; }
        public int Age;
        public int Course;

        public override string ToString()
        {
            return $"{LastName}|{Initials}|{Birthday.ToString()}|{EnterDate.ToString()}|{IndexGroup}|{Faculty}|{Specialization}|{AverageRating}";
        }
        public Student()
        {

        }
    }
}
