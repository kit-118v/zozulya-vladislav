﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab1
{
    public class Menu
    {
        Students students;

        public void MainMenu()
        {
            students = new Students();
            students.createArr(0);
            string selection;
            int number = 0;

            while (number != 1)
            {
                settings();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            readStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            showAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void settings()
        {
            Console.WriteLine("Amount of students: " + students.getLength());
            Console.WriteLine("1 - exit");
            Console.WriteLine("2 - add new student");
            Console.WriteLine("3 - show all students");
            Console.Write("Your choice: ");
        }

        void readStudent()
        {
            var student = new Student();

            student.LastName = checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
            student.Initials = checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
            student.Birthday = checkDate("Enter date of birthday [only 'YYYY-MM-DD' format]: ");
            student.EnterDate = checkDate("Enter date of adding student to list [only 'YYYY-MM-DD' format]:");
            student.IndexGroup = checkRegex("Enter index of group: ", @"^[A-Za-z]$");
            student.Faculty = checkRegex("Enter faculty: ", @"^.*");
            student.Specialization = checkRegex("Enter specialization: ", @"^.*");
            student.AverageRating = Convert.ToInt32(checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));
            
            students.add(student);
        }
        DateTime checkDate(string text)
        {
            DateTime dt = new DateTime();
            try
            {
                Console.WriteLine(text);
                string entered = Console.ReadLine();
                return DateTime.ParseExact(entered, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            catch (System.FormatException e)
            {
                Console.WriteLine("Invalid date. ");
            }
            return dt;
        }
        public string checkRegex(string text, string regex)
        {
            string str = null;

            Console.Write(text);
            while (true)
            {
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, regex))
                {
                    Console.Write("Invalid input. Try again: ");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return str;
        }

        void showAll()
        {
            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            var strs = students.printAll();
            string[] infoStudent;
            foreach (string str in strs)
            {
                infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
            }
            Console.WriteLine("------------------------------------------------------");
        }
    }
}