using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text.RegularExpressions;

namespace Lab1Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string[] input = { "zoz", "Zoz", "123", "ZOZ", "_zoZ" };
            Boolean[] expected = { false, true, false, false, false };
            Boolean actual;
            for (int i = 0; i < input.Length; i++)
            {
                actual = Regex.IsMatch(input[i], @"^[A-Z][a-z]+$");
                Assert.AreEqual(expected[i], actual);
                Console.WriteLine("Expected [" + expected[i] + "]    Actual [" + actual + "]");
            }
        }
    }
}
