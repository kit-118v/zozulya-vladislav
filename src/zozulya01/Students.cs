﻿using System;

namespace Lab1
{

    class Students
    {
        Student[] _students;
        public void createArr(int size)
        {
            if (size >= 0)
            {
                _students = new Student[size];
            } else
            {
                Console.WriteLine("Error: cannot create an array.\n");
            }
        }

        public void add(Student newStudent)
        {
            Student[] newStudents = new Student[_students.Length + 1];
            if (_students.Length > 0)
            {
                Array.Copy(_students, newStudents, _students.Length);
            }
            newStudents[newStudents.Length - 1] = newStudent;
            _students = newStudents;
        }

        public string[] printAll()
        {
            string[] strs = new string[_students.Length];
            int i = 0;
            foreach (Student student in _students)
            {
                strs[i] = student.ToString();
                i++;
            }
            return strs;
        }
        public int getLength()
        {
            return _students.Length;
        }
        public Students()
        {

        }
    }

}
