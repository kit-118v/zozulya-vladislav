﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab3
{
    public class Student
    {
        public string LastName { get; set; }
        public string Initials { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime EnterDate { get; set; }
        public string IndexGroup { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int AverageRating { get; set; }

        public override string ToString()
        {
            return $"{LastName}|{Initials}|{Birthday.ToString()}|{EnterDate.ToString()}|{IndexGroup}|{Faculty}|{Specialization}|{AverageRating}";
        }

        public Student()
        {

        }
    }
}
