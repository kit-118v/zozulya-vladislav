using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab3;
namespace Lab3Tests
{
    [TestClass]
    public class MyCollectionTest
    {
        [TestMethod]
        public void testWriteRead()
        {
            Menu menu = new Menu();
            
            var obj = new Student();
            obj.LastName = "Zozulya";
            obj.Initials = "V.D.";
            obj.Birthday = new System.DateTime(2001, 3, 6);
            obj.EnterDate = new System.DateTime(2020, 12, 10);
            obj.IndexGroup = "v";
            obj.Faculty = "Programming engineering";
            obj.Specialization = "Fixer";
            obj.AverageRating = 89;
            menu.students.Add(obj);

            menu.WriteToFile();
            menu.students.clear();
            menu.ReadFromFile();


            Assert.AreEqual(obj.LastName, menu.students.get(0).LastName, "LastName isn't valid");
            Assert.AreEqual(obj.Initials, menu.students.get(0).Initials, "Initials isn't valid");
            Assert.AreEqual(obj.Birthday, menu.students.get(0).Birthday, "Birthday isn't valid");
            Assert.AreEqual(obj.EnterDate, menu.students.get(0).EnterDate, "EnterDate isn't valid");
            Assert.AreEqual(obj.IndexGroup, menu.students.get(0).IndexGroup, "IndexGroup isn't valid");
            Assert.AreEqual(obj.Faculty, menu.students.get(0).Faculty, "Faculty isn't valid");
            Assert.AreEqual(obj.Specialization, menu.students.get(0).Specialization, "Specialization isn't valid");
            Assert.AreEqual(obj.AverageRating, menu.students.get(0).AverageRating, "AverageRating isn't valid");

        }
    }
}