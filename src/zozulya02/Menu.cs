﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Lab2
{
    class Menu
    {
        MyCollection<Student> students;

        public void MainMenu()
        {
            students = new MyCollection<Student>();
            string selection;
            var number = 0;

            while (number != 1)
            {
                Settings();
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            ReadStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            ShowAll();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            PrintByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            RemoveByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void Settings()
        {
            Console.WriteLine("Amount of students: " + students.Count());
            Console.WriteLine("1 - exit");
            Console.WriteLine("2 - add new student");
            Console.WriteLine("3 - show all students");
            Console.WriteLine("4 - get student by index");
            Console.WriteLine("5 - remove student by index");
            Console.Write("Your choice: ");
        }

        void ReadStudent()
        {
            var student = new Student();

            student.LastName = checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
            student.Initials = checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
            student.Birthday = checkDate("Enter date of birthday [only 'YYYY-MM-DD' format]: ");
            student.EnterDate = checkDate("Enter date of adding student to list [only 'YYYY-MM-DD' format]:");
            student.IndexGroup = checkRegex("Enter index of group: ", @"^[A-Za-z]$");
            student.Faculty = checkRegex("Enter faculty: ", @"^.*");
            student.Specialization = checkRegex("Enter specialization: ", @"^.*");
            student.AverageRating = Convert.ToInt32(checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));

            students.Add(student);
        }
        DateTime checkDate(string text)
        {
            DateTime dt = new DateTime();
            try
            {
                Console.WriteLine(text);
                string entered = Console.ReadLine();
                return DateTime.ParseExact(entered, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            catch (System.FormatException e)
            {
                Console.WriteLine("Invalid date. ");
            }
            return dt;
        }
        public string checkRegex(string text, string regex)
        {
            string str = null;

            Console.Write(text);
            while (true)
            {
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, regex))
                {
                    Console.Write("Invalid input. Try again: ");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return str;
        }
        void ShowAll()
        {
            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            string str;
            string[] infoStudent;
            var i = 0;
            foreach (var student in students)
            {
                str = student.ToString();
                infoStudent = str.Split(new char[] { '|' });

                Console.WriteLine("Index of student: " + i);
                Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
                Console.WriteLine("Birthday: " + infoStudent[2]);
                Console.WriteLine("Enter date: " + infoStudent[3]);
                Console.WriteLine("Inedx of group: " + infoStudent[4]);
                Console.WriteLine("Faculty: " + infoStudent[5]);
                Console.WriteLine("Specialization: " + infoStudent[6]);
                Console.WriteLine("Average rating: " + infoStudent[7]);
                Console.WriteLine("");
                i++;
            }

            Console.WriteLine("------------------------------------------------------");
        }

        void PrintByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= students.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var str = students[index].ToString();
            var infoStudent = str.Split(new char[] { '|' });

            Console.WriteLine("-----------------------Students-----------------------");
            Console.WriteLine("");
            Console.WriteLine("Index of student: " + index);
            Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
            Console.WriteLine("Birthday: " + infoStudent[2]);
            Console.WriteLine("Enter date: " + infoStudent[3]);
            Console.WriteLine("Inedx of group: " + infoStudent[4]);
            Console.WriteLine("Faculty: " + infoStudent[5]);
            Console.WriteLine("Specialization: " + infoStudent[6]);
            Console.WriteLine("Average rating: " + infoStudent[7]);
            Console.WriteLine("");
            Console.WriteLine("------------------------------------------------------");
        }
        void RemoveByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());

            if (students.remove(index))
                Console.WriteLine("Student was successfully deleted. ");
            else
                Console.WriteLine("Failed. ");
        }
    }
}