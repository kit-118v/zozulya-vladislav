using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab2;
namespace Lab2Tests
{
    [TestClass]
    public class MyCollectionTest
    {
        [TestMethod]
        public void testCount()
        {
            var myCollection = new MyCollection<Student>();
            Assert.AreEqual(0, myCollection.Count(), "Count method isn't valid");
        }

        [TestMethod]
        public void testAdd()
        {
            var myCollection = new MyCollection<Student>();
            var obj = new Student();
            obj.LastName = "Zozulya";
            obj.Initials = "V.D.";
            obj.Birthday = new System.DateTime(2001, 3, 6);
            obj.EnterDate = new System.DateTime(2020, 12, 11);
            obj.IndexGroup = "v";
            obj.Faculty = "Programming engineering";
            obj.Specialization = "Programmer";
            obj.AverageRating = 89;
            myCollection.Add(obj);

            var expectedObj = new Student();
            expectedObj.LastName = "Zozulya";
            expectedObj.Initials = "V.D.";
            expectedObj.Birthday = new System.DateTime(2001, 3, 6);
            expectedObj.EnterDate = new System.DateTime(2020, 12, 11);
            expectedObj.IndexGroup = "v";
            expectedObj.Faculty = "Programming engineering";
            expectedObj.Specialization = "Programmer";
            expectedObj.AverageRating = 89;

            Assert.AreEqual(expectedObj.LastName, myCollection[0].LastName, "LastName isn't valid");
            Assert.AreEqual(expectedObj.Initials, myCollection[0].Initials, "Initials isn't valid");
            Assert.AreEqual(expectedObj.Birthday, myCollection[0].Birthday, "Birthday isn't valid");
            Assert.AreEqual(expectedObj.EnterDate, myCollection[0].EnterDate, "EnterDate isn't valid");
            Assert.AreEqual(expectedObj.IndexGroup, myCollection[0].IndexGroup, "IndexGroup isn't valid");
            Assert.AreEqual(expectedObj.Faculty, myCollection[0].Faculty, "Faculty isn't valid");
            Assert.AreEqual(expectedObj.Specialization, myCollection[0].Specialization, "Specialization isn't valid");
            Assert.AreEqual(expectedObj.AverageRating, myCollection[0].AverageRating, "AverageRating isn't valid");
        }

        [TestMethod]
        public void testForeach()
        {
            var myCollection = new MyCollection<Student>();
            var recieved = new MyCollection<Student>();

            var obj1 = new Student();
            obj1.LastName = "Zozulya";
            obj1.Initials = "V.D.";
            obj1.Birthday = new System.DateTime(2001, 8, 1);
            obj1.EnterDate = new System.DateTime(2018, 9, 1);
            obj1.IndexGroup = "v";
            obj1.Faculty = "Programming engineering";
            obj1.Specialization = "System administrator";
            obj1.AverageRating = 89;
            myCollection.Add(obj1);

            var obj2 = new Student();
            obj2.LastName = "Kisliy";
            obj2.Initials = "V.V.";
            obj2.Birthday = new System.DateTime(1971, 12, 15);
            obj2.EnterDate = new System.DateTime(2016, 9, 1);
            obj2.IndexGroup = "a";
            obj2.Faculty = "Programming engineering";
            obj2.Specialization = "WEB-developer";
            obj2.AverageRating = 90;
            myCollection.Add(obj2);

            foreach (var obj in myCollection)
            {
                recieved.Add(obj);
            }

            var i = 0;
            foreach (var obj in myCollection)
            {
                Assert.AreEqual(obj.LastName, recieved[i].LastName, "LastName isn't valid");
                Assert.AreEqual(obj.Initials, recieved[i].Initials, "Initials isn't valid");
                Assert.AreEqual(obj.Birthday, recieved[i].Birthday, "Birthday isn't valid");
                Assert.AreEqual(obj.EnterDate, recieved[i].EnterDate, "EnterDate isn't valid");
                Assert.AreEqual(obj.IndexGroup, recieved[i].IndexGroup, "IndexGroup isn't valid");
                Assert.AreEqual(obj.Faculty, recieved[i].Faculty, "Faculty isn't valid");
                Assert.AreEqual(obj.Specialization, recieved[i].Specialization, "Specialization isn't valid");
                Assert.AreEqual(obj.AverageRating, recieved[i].AverageRating, "AverageRating isn't valid");
                i++;
            }
        }




        [TestMethod]
        public void testRemove()
        {
            var myCollection = new MyCollection<Student>();
            var recieved = new MyCollection<Student>();

            var obj1 = new Student();
            obj1.LastName = "Zozulya";
            obj1.Initials = "V.D.";
            obj1.Birthday = new System.DateTime(2001, 8, 1);
            obj1.EnterDate = new System.DateTime(2018, 9, 1);
            obj1.IndexGroup = "v";
            obj1.Faculty = "Programming engineering";
            obj1.Specialization = "System administrator";
            obj1.AverageRating = 89;
            myCollection.Add(obj1);

            var obj2 = new Student();
            obj2.LastName = "Kisliy";
            obj2.Initials = "V.V.";
            obj2.Birthday = new System.DateTime(1971, 12, 15);
            obj2.EnterDate = new System.DateTime(2016, 9, 1);
            obj2.IndexGroup = "a";
            obj2.Faculty = "Programming engineering";
            obj2.Specialization = "WEB-developer";
            obj2.AverageRating = 90;
            myCollection.Add(obj2);

            myCollection.remove(0);
            foreach (var obj in myCollection)
            {
                recieved.Add(obj);
            }

            Assert.AreEqual(myCollection[0].LastName, recieved[0].LastName, "LastName isn't valid");
            Assert.AreEqual(myCollection[0].Initials, recieved[0].Initials, "Initials isn't valid");
            Assert.AreEqual(myCollection[0].Birthday, recieved[0].Birthday, "Birthday isn't valid");
            Assert.AreEqual(myCollection[0].EnterDate, recieved[0].EnterDate, "EnterDate isn't valid");
            Assert.AreEqual(myCollection[0].IndexGroup, recieved[0].IndexGroup, "IndexGroup isn't valid");
            Assert.AreEqual(myCollection[0].Faculty, recieved[0].Faculty, "Faculty isn't valid");
            Assert.AreEqual(myCollection[0].Specialization, recieved[0].Specialization, "Specialization isn't valid");
            Assert.AreEqual(myCollection[0].AverageRating, recieved[0].AverageRating, "AverageRating isn't valid");
        }
    }
}
