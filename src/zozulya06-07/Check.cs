﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Lab6
{
    class Check
    {
        public static DateTime checkDate(string text)
        {
            DateTime dt = new DateTime();
            try
            {
                Console.WriteLine(text);
                string entered = Console.ReadLine();
                return DateTime.ParseExact(entered, "MM/DD/YYYY", CultureInfo.InvariantCulture);
            }
            catch (System.FormatException e)
            {
                Console.WriteLine("Invalid date. ");
            }
            return dt;
        }
        public static string checkRegex(string text, string regex)
        {
            string str = null;

            Console.Write(text);
            while (true)
            {
                str = Console.ReadLine();
                if (!Regex.IsMatch(str, regex))
                {
                    Console.Write("Invalid input. Try again: ");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return str;
        }
    }
}
