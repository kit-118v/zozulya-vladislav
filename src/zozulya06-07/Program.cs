﻿using System;
using System.Globalization;
using System.Linq;


namespace Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Menu menu = new Menu();
            //menu.MainMenu();
            DateTime today = DateTime.Today;
            DateTime b = DateTime.ParseExact("10 12 2000", "dd MM yyyy", CultureInfo.InvariantCulture);

            TimeSpan old = today.Subtract(b);
            var d = new DateTime(old.Ticks);
            int year = Convert.ToInt32(Math.Floor(old.TotalDays / 365));
            int month = Convert.ToInt32(Math.Floor((old.TotalDays % 365) / 31));
            int day = Convert.ToInt32(Math.Floor((old.TotalDays % 365) % 31));

            Console.WriteLine($"Возраст: {year} лет, {month} месяцев, {day} дней");
        }
    }
}