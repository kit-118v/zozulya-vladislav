﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Lab6
{
    public class Menu
    {
        public MyCollection<Student> students;

        public Menu()
        {
            students = new MyCollection<Student>();
        }
        public void MainMenu()
        {
            string selection;
            var number = 0;

            while (number != 1)
            {
                PrintHelper.printMainMenu(students);
                selection = Console.ReadLine();

                if (int.TryParse(selection, out number))
                {
                    switch (number)
                    {
                        case 2:
                            Console.Clear();
                            ReadStudent();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            PrintHelper.PrintAll(students);
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            PrintHelper.PrintByIndex(students);
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            Console.Clear();
                            File_Serialization.WriteToFile(students);
                            break;
                        case 6:
                            Console.Clear();
                            students.clear();
                            students = File_Serialization.ReadFromFile();
                            break;
                        case 7:
                            Console.Clear();
                            Edit.EditStudent(students);
                            Console.Clear();
                            break;
                        case 8:
                            Console.Clear();
                            RemoveByIndex();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 9:
                            Console.Clear();
                            students.clear();
                            Console.Clear();
                            break;
                        case 10:
                            Console.Clear();
                            SearchByLastname();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 11:
                            Console.Clear();
                            Lab4Info();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 12:
                            Console.Clear();
                            Filter.selectItems(students);
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 13:
                            Console.Clear();
                            File_Serialization.Save(students);
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 14:
                            Console.Clear();
                            students.clear();
                            students = File_Serialization.Load();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                    }
                }
            }
        }

        void Lab4Info()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index < 0 || index >= students.Count())
            {
                return;
            }
            Console.WriteLine("Enterence date: " + students.get(index).EnterDate);
            Console.WriteLine("Faculty: " + students.get(index).Faculty);
            Console.WriteLine("Specialization: " + students.get(index).Specialization);
            Console.WriteLine("Group index: " + students.get(index).IndexGroup);

            string s = string.Format("{0}-{1}{2}", students.get(index).Faculty, students.get(index).EnterDate.Year, students.get(index).IndexGroup);

            Console.WriteLine("\nGroup: " + s);
            Console.WriteLine("\nCourse: " + students.get(index).Course);
            Console.WriteLine("\nAge: " + students.get(index).Age);
        }

        void SearchByLastname()
        {
            Console.WriteLine("Enter lastname: ");
            string str = Console.ReadLine();
            int i = 0, k = 0;
            foreach (var student in students)
            {
                if (student.LastName.IndexOf(str) >= 0)
                {
                    PrintHelper.printStudent(student.ToString(), i);
                    k++;
                }
                i++;
            }
            if (k == 0)
                Console.WriteLine("Nothing found. ");
        }



        void ReadStudent()
        {
            var student = new Student();

            student.LastName = Check.checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
            student.Initials = Check.checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
            student.Birthday = Check.checkDate("Enter date of birthday [only 'MM/DD/YYYY' format]: ");
            student.EnterDate = Check.checkDate("Enter date of adding student to list [only 'MM/DD/YYYY' format]:");
            student.IndexGroup = Check.checkRegex("Enter index of group: ", @"^[A-Za-z]$");
            student.Faculty = Check.checkRegex("Enter faculty: ", @"^.*");
            student.Specialization = Check.checkRegex("Enter specialization: ", @"^.*");
            student.AverageRating = Convert.ToInt32(Check.checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));

            students.Add(student);
        }

        void RemoveByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());

            if (students.remove(index))
                Console.WriteLine("Student was successfully deleted. ");
            else
                Console.WriteLine("Failed. ");
        }
    }
}