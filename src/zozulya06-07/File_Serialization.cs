﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Lab6
{
    public class File_Serialization
    {
        public static void WriteToFile(MyCollection<Student> students)
        {
            var path = @"D:\students.txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in students)
                    {
                        sw.WriteLine(student.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static MyCollection<Student> ReadFromFile()
        {
            MyCollection<Student> students = new MyCollection<Student>();
            var path = @"D:\students.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });
                        var obj = new Student();
                        obj.LastName = infoStudent[0];
                        obj.Initials = infoStudent[1];
                        obj.SetBirthday(DateTime.Parse(infoStudent[2]));
                        obj.SetEnterDate(DateTime.Parse(infoStudent[3]));
                        obj.IndexGroup = infoStudent[4];
                        obj.Faculty = infoStudent[5];
                        obj.Specialization = infoStudent[6];
                        obj.AverageRating = int.Parse(infoStudent[7]);
                        students.Add(obj);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return students;
        }
        public static void Save(MyCollection<Student> students)
        {
            var path = @"D:\people.dat";
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, students);
                Console.WriteLine("Serialization completed. ");
            }
        }

        public static MyCollection<Student> Load()
        {
            var path = @"D:\people.dat";
            MyCollection<Student> students;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                students = (MyCollection<Student>)formatter.Deserialize(fs);
                Console.WriteLine("Deserialization completed. ");
            }
            return students;
        }
    }
}
