﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lab6
{
    class Filter
    {
        delegate void Calc();

        private static MyCollection<Student> selected_students;
        private static IEnumerable<Student> selected_students2;
        public static void selectItems(MyCollection<Student> students)
        {
            int index = 0;
            int k = 0;
            string str;
            selected_students = new MyCollection<Student>();
            string header = "   Name  | Init |        Birthday        |        EnterDate       |Inx|       Faculty       |  Specialization  | Avg | Age |\n---------|------|------------------------|------------------------|---|---------------------|------------------|-----|-----|";

            if (students.Count() == 0)
            {
                Console.WriteLine("The list of students is empty. ");
                Console.ReadLine();
                return;
            }

            PrintHelper.printFilterMenu();
            while (true)
            {
                Console.Write("Choose: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index >= 2 && index <= 13)
                    break;
                else if (index == 1)
                    return;
            }
            //  2, 3, 4      0  1  2
            //  5, 6, 7      3  4  5
            //  8, 9,10      6  7  8
            // 11,12,13      9 10 11
            switch ( (index - 2) % 3)
            {
                case 0:
                    Console.WriteLine("Enter group index: ");
                    str = Console.ReadLine();
                    Console.WriteLine(header);
                    //selected_students2 = students.Cast<Student>().Where(ass => ass.IndexGroup.IndexOf(str) >= 0).Select(ass => ass);

                    //отложенное выполнение запроса
                    selected_students2 = from s in students where s.IndexGroup.IndexOf(str)>=0 select s; 
                    foreach (var student in selected_students2)
                    {
                        selected_students.Add(student);
                        k++;
                    }
                    break;
                case 1:
                    Console.WriteLine("Enter faculty: ");
                    str = Console.ReadLine();
                    Console.WriteLine(header);
                    //selected_students2 = students.Cast<Student>().Where(ass => ass.Faculty.IndexOf(str) >= 0).Select(ass => ass);

                    //принудительное выполнение запроса
                    selected_students2 = (from s in students where s.Faculty.IndexOf(str)>=0 select s).ToList<Student>(); 
                    foreach (var student in selected_students2)
                    {
                        selected_students.Add(student);
                        k++;
                    }
                    break;
                case 2:
                    Console.WriteLine("Enter specialization: ");
                    str = Console.ReadLine();
                    Console.WriteLine(header);

                    //лямбда
                    selected_students2 = students.Cast<Student>().Where(ass => ass.Specialization.IndexOf(str) >= 0).Select(ass => ass); 
                    foreach (var student in selected_students2)
                    {
                        selected_students.Add(student);
                        k++;
                    }
                    break;
            }
            if (k == 0)
                Console.WriteLine("Nothing found. ");
            else
            {
                if (index >= 2 && index <= 4)
                {
                    //print
                    showSelectedItems();
                }
                else if (index >= 5 && index <= 7)
                {
                    //remove
                    showSelectedItems();
                    removeSelectedStudents(students);
                }
                else if (index >= 8 && index <= 13)
                {
                    //calc
                    showSelectedItems();
                    calculateSelectedItems(index);
                }
            }
        }

        private static void removeSelectedStudents(MyCollection<Student> students)
        {
            string str;
            Console.WriteLine("Do you want to delete selected items? (y/n): ");
            str = Console.ReadLine();
            while (string.Compare(str, "n") != 0 && string.Compare(str, "y") != 0)
            {
                Console.WriteLine("Try again: ");
                str = Console.ReadLine();
            }

            if (string.Compare(str, "y") == 0)
            {
                if (students.removeAll(selected_students))
                    Console.WriteLine("Deleted successfully. ");
                else
                    Console.WriteLine("Error. ");
            }
            else
            {
                Console.WriteLine("Cancel. ");
            }
        }

        private static void showSelectedItems()
        {
            foreach (var student in selected_students)
            {
                Console.WriteLine(student.toRow());
            }
        }

        private static void calculateSelectedItems(int index)
        {
            Calc calc;
            if (index == 8 || index == 9 || index == 10)
                calc = calcAvgAge;
            else
                calc = calcAvgRating;
            calc();
        }

        private static void calcAvgAge()
        {
            //double avg = 0;
            //foreach (var student in selected_students)
            //avg += student.Age;
            //Console.WriteLine("Average age = " + avg/selected_students.Count());

            //вызов статистического запроса
            double list = (from s in selected_students select s.Age).Average(); 
            Console.WriteLine("Average age = " + list);
        }

        private static void calcAvgRating()
        {
            //double avg = 0;
            //foreach (var student in selected_students)
               // avg += student.AverageRating;
            //Console.WriteLine("Average rating = " + avg / selected_students.Count());

            double list = (from s in selected_students select s.AverageRating).Average(); //вызов статистического запроса
            Console.WriteLine("Average age = " + list);
        }

    }
}