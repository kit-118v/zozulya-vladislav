﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5
{
    class Edit
    {
        public static void EditStudent(MyCollection<Student> students)
        {
            if (students.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            int number = 0;
            int index = 0;
            while (true)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < students.Count() && index >= 0)
                {
                    break;
                }
            }

            PrintHelper.printEditMenu();
            selection = Console.ReadLine();

            if (int.TryParse(selection, out number))
            {
                switch (number)
                {
                    case 1:
                        return;
                    case 2:
                        students[index].LastName = Check.checkRegex("Enter surname of student: ", @"^[A-Z][a-z]+$");
                        break;
                    case 3:
                        students[index].Initials = Check.checkRegex("Enter initials [only 'X.X.' format]: ", @"[A-Z][.][A-Z][.]$");
                        break;
                    case 4:
                        students[index].Birthday = Check.checkDate("Enter date of birthday [only 'YYYY-MM-DD' format]: ");
                        break;
                    case 5:
                        students[index].EnterDate = Check.checkDate("Enter date of adding student to list [only 'YYYY-MM-DD' format]:");
                        break;
                    case 6:
                        students[index].IndexGroup = Check.checkRegex("Enter index of group: ", @"^[A-Za-z]$");
                        break;
                    case 7:
                        students[index].Faculty = Check.checkRegex("Enter faculty: ", @"^.*");
                        break;
                    case 8:
                        students[index].Specialization = Check.checkRegex("Enter specialization: ", @"^.*");
                        break;
                    case 9:
                        students[index].AverageRating = Convert.ToInt32(Check.checkRegex("Enter average rating: ", @"^(?:[0-9][0-9]?|100)$"));
                        break;
                }

                Console.WriteLine("The result of redaction: ");
                PrintHelper.printStudent(students[index].ToString(), index);
            }
        }
    }
}
