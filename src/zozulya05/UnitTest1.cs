using Lab5;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Lab5Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MyCollection<Student> students = new MyCollection<Student>();

            Student s1 = new Student();
            s1.LastName = "Zozulya";
            s1.Initials = "V.D.";
            s1.SetBirthday(DateTime.Parse("3/6/2001 12:00:00 AM"));
            s1.SetEnterDate(DateTime.Parse("9/1/2018 12:00:00 AM"));
            s1.IndexGroup = "v";
            s1.Faculty = "System Programming";
            s1.Specialization = "Programmer";
            s1.AverageRating = int.Parse("89");
            students.Add(s1);

            Student s2 = new Student();
            s2.LastName = "Morozova";
            s2.Initials = "A.I.";
            s2.SetBirthday(DateTime.Parse("10/20/2000 12:00:00 AM"));
            s2.SetEnterDate(DateTime.Parse("9/1/2017 12:00:00 AM"));
            s2.IndexGroup = "a";
            s2.Faculty = "System Programming";
            s2.Specialization = "Programmer";
            s2.AverageRating = int.Parse("99");
            students.Add(s2);

            File_Serialization.Save(students);
            students.clear();
            students = File_Serialization.Load();


            Assert.AreEqual(s1, students.get(0));
            Assert.AreEqual(s2, students.get(1));
        }
    }
}