﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5
{
    class PrintHelper
    {
        public static void printMainMenu(MyCollection<Student> students)
        {
            Console.WriteLine("Amount of students: " + students.Count());
            Console.WriteLine("1  - Exit");
            Console.WriteLine("2  - Add new student");
            Console.WriteLine("3  - Show all students");
            Console.WriteLine("4  - Get student by index");
            Console.WriteLine("5  - Save in file");
            Console.WriteLine("6  - Read from file");
            Console.WriteLine("7  - Edit student");
            Console.WriteLine("8  - Delete student");
            Console.WriteLine("9  - Delete all students");
            Console.WriteLine("10 - Search by lastname");
            Console.WriteLine("11 - Show LAB4 info");
            Console.WriteLine("12 - Filter by faculty, spec., group");
            Console.WriteLine("13 - Serialize");
            Console.WriteLine("14 - Deserialize");
            Console.Write("Your choice: ");
        }
        public static void printEditMenu()
        {
            Console.WriteLine("1 - Exit");
            Console.WriteLine("Edit: ");
            Console.WriteLine("  2 - Name");
            Console.WriteLine("  3 - Initials");
            Console.WriteLine("  4 - Birthday");
            Console.WriteLine("  5 - Enter date");
            Console.WriteLine("  6 - Index of group");
            Console.WriteLine("  7 - Faculty");
            Console.WriteLine("  8 - Specialization");
            Console.WriteLine("  9 - Average rating");
        }
        public static void printFilterMenu()
        {
            Console.WriteLine("1 - Exit");
            Console.WriteLine("Print by: ");
            Console.WriteLine("  2  - Group");
            Console.WriteLine("  3  - Faculty");
            Console.WriteLine("  4  - Specialization");
            Console.WriteLine("Delete by: ");
            Console.WriteLine("  5  - Group");
            Console.WriteLine("  6  - Faculty");
            Console.WriteLine("  7  - Specialization");
            Console.WriteLine("Calculate average age by: ");
            Console.WriteLine("  8  - Group");
            Console.WriteLine("  9  - Faculty");
            Console.WriteLine("  10 - Specialization");
            Console.WriteLine("Calculate average rating by: ");
            Console.WriteLine("  11 - Group");
            Console.WriteLine("  12 - Faculty");
            Console.WriteLine("  13 - Specialization");
        }

        public static void printStudent(string str, int index)
        {
            string[] infoStudent = str.Split(new char[] { '|' });
            Console.WriteLine("Index of student: " + index);
            Console.WriteLine("Name: " + infoStudent[0] + ' ' + infoStudent[1]);
            Console.WriteLine("Birthday: " + infoStudent[2]);
            Console.WriteLine("Enter date: " + infoStudent[3]);
            Console.WriteLine("Inedx of group: " + infoStudent[4]);
            Console.WriteLine("Faculty: " + infoStudent[5]);
            Console.WriteLine("Specialization: " + infoStudent[6]);
            Console.WriteLine("Average rating: " + infoStudent[7]);
            Console.WriteLine("");
        }
        public static void PrintByIndex(MyCollection<Student> students)
        {
            int index;
            Console.Write("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= students.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            printStudent(students[index].ToString(), index);
        }
        public static void PrintAll(MyCollection<Student> students)
        {
            int i = 0;
            Console.WriteLine("-----------------------Students-----------------------");
            foreach (var student in students)
            {
                PrintHelper.printStudent(student.ToString(), i);
                i++;
            }
        }
    }
}
